package com.javacode2018.springmvc.chat09.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 公众号：路人甲java，专注于java干货分享
 * 个人博客：http://itsoku.com/
 * 已推出的系列有：【spring系列】、【java高并发系列】、【MySQL系列】、【MyBatis系列】、【Maven系列】
 * git地址：https://gitee.com/javacode2018
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @RequestMapping("/login")
    public String login() {
        return "login view";
    }

    @RequestMapping("/add")
    public String add() {
        return "add view";
    }

    @RequestMapping("/del")
    public String modify() {
        return "modify view";
    }

    @RequestMapping("/list")
    public String list() {
        return "list view";
    }
}
